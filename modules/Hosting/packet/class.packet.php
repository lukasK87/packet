<?php

/**
 * Class hetznercloud
 *
 * Hosting/Provisioning module
 * @see  http://dev.hostbillapp.com/dev-kit/provisioning-modules/
 * @author HostBill <cs@hostbillapp.com>
 *
 */
class packet extends HostingModule {


    protected $_repository = 'hosting_packet';

    /**
     * Module version. Make sure to increase version any time you add some
     * new functions in this class!
     * @var string
     */
    protected $version = '2.200999';

    /**
     * Module name, visible in admin portal.
     * @var string
     */
    protected $modname = 'packet';

    /**
     * Module description, visible in admin portal
     * @var string
     */
    protected $description = 'Packet module for HostBill';

    /**
     * Connected instance of PDO object
     * @var PDO
     */
    protected $db;

    /**
     * You can choose which fields to display in Settings->Apps section
     * by defining this variable
     * @var array
     */
    protected $serverFields = [
        self::CONNECTION_FIELD_USERNAME => true,
        self::CONNECTION_FIELD_PASSWORD => false,
        self::CONNECTION_FIELD_INPUT1 => true,
        self::CONNECTION_FIELD_INPUT2 => false,
        self::CONNECTION_FIELD_CHECKBOX => false,
        self::CONNECTION_FIELD_HOSTNAME => false,
        self::CONNECTION_FIELD_IPADDRESS => false,
        self::CONNECTION_FIELD_MAXACCOUNTS => false,
        self::CONNECTION_FIELD_STATUSURL => false,
        self::CONNECTION_FIELD_TEXTAREA => false,
    ];

    protected $serverFieldsDescription = [
        self::CONNECTION_FIELD_USERNAME => 'API Key',
        self::CONNECTION_FIELD_INPUT1 => 'Project ID'
    ];


    /**
     * options for the product configuration from Settings => Products & Services => Product => Connect with Module
     * @var array
     */
    protected $options = [
        'Location' => [
            'value' => '',
            'description' => '',
            'type' => 'loadable',
            'default' => 'getLocations',
            'forms'=>'select',
            'variable' => 'location'
        ],
        'Server Type' => [
            'value' => '',
            'description' => '',
            'type' => 'loadable',
            'default' => 'getServerTypes',
            'forms'=>'select',
            'variable' => 'type'
        ],
        'Operating System' => [
            'value' => '',
            'description' => '',
            'type' => 'loadable',
            'default' => 'getAvailableOS',
            'forms'=>'select',
            'variable' => 'os'
        ],
        'SSH key' => [
            'value' => '',
            'description' => '',
            'type' => 'sshkeyselect',
            'default' => '',
            'forms'=>'sshkeyselect',
            'variable' => 'sshkey'
        ]

    ];

     /**
     * This is helped variable to store data passed in connect
     * @see connect()
     * @ignore
     * @var string
     */
    private $api_key;
    private $project_id;

    protected $details = [
        'device_id' =>[
            'name'=> 'UUID',
            'value' => false,
            'type'=> 'input',
            'default'=>false
        ]
    ];

    /**
     * hetznercloud constructor.
     */
    public function __construct(){
        parent::__construct();
    }

    public function install() {

    }


    /**
     * HostBill will call this method before calling any other function from your module
     * It will pass remote  app details that module should connect with
     *
     * @param array $connect Server details configured in Settings->Apps
     */
    public function connect($connect) {

        $this->setApiKey($connect['username']);
        if($connect['field1']){
            $this->setProjectId($connect['field1']);
        }
        return $connect;
    }

    public function setApiKey($key){
        $this->api_key = $key;
    }

    public function getApiKey(){
        return $this->api_key;
    }

    public function setProjectId($project_id){
        $this->project_id = $project_id;
    }

    public function getProjectId(){
        return $this->project_id;
    }
    /**
     * HostBill will call this method when admin clicks on "test Connection" in settings->apps
     * It should test connection to remote app using details provided in connect method
     *
     * Use $this->addError('message'); to provide errors details (if any)
     *
     * @see connect
     * @return boolean true if connection suceeds
     */
    public function testConnection() {
        return $this->call('user', 'GET');

    }


    public function call($endpoint, $method='GET', $data=''){
        $base_url = 'https://api.packet.net/';

        $url = $base_url.''.$endpoint;

        $curl = curl_init();
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => [
                'X-Auth-Token: '.$this->getApiKey(),

            ],
        ];


        if($data AND is_array($data)){
            $options[CURLOPT_POSTFIELDS] = json_encode($data);
            $options[CURLOPT_HTTPHEADER][] = "Content-Type: application/json";

        }

        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);
        $err = curl_error($curl);



        curl_close($curl);

        if ($err) {
            $this->addError('Connection error: '.$err);
            return false;
        } else {
            $response = json_decode($response, 1);

            if($response['id'] AND $endpoint=='user') {

                return true;
            }
            elseif($response['error'] AND $response['error'][0]!='Key already exists'){
                $this->addError($response['error']);
                return false;
            }
            else {
                return $response;
            }

        }

    }

    public function getProject($id_project=0){

        $endpoint = 'projects';

        if($id_project){
            $endpoint .='/'.$id_project ;
        }

        return $this->call($endpoint);
    }

    public function getLocations($id_project = 0){
        $endpoint = 'facilities';

        if($id_project){
            $endpoint = 'projects/'.$id_project.'/'.$endpoint;
        }

        $facilities = $this->call($endpoint);

        $return = [];

        foreach($facilities['facilities'] as $v) {
            $return[] = [$v['id'],$v['name']];
        }
        return $return;
    }

    public function getServerTypes($id_project = ''){

        if($id_project){
            $endpoint = 'projects/'.$id_project.'/plans';
        }
        else{
            $endpoint = 'plans';
        }

        $all = $this->call($endpoint);

        $plans = [];

        foreach($all['plans'] AS $plan){
            $plans[] = [$plan['id'], $plan['name']];

        }

        return $plans;

    }

    public function getAvailableOS($get_all_info = false){
            $endpoint = 'operating-systems';
            $operating_systems = $this->call($endpoint);

            if($get_all_info){
                return $operating_systems['operating_systems'];
            }

            $result = [];

            foreach($operating_systems['operating_systems'] AS $sys){
                $result[] = [$sys['id'], $sys['name']];
            }


            return $result;
    }

    public function getProductServers($product_id = 0){
        if (empty($product_id)){
            return false;
        }

        $query = $this->db->prepare("SELECT `server` FROM hb_products_modules WHERE `product_id` = :product_id");
        $query->execute(array('product_id' => $product_id));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();

        if (!$result){
            return false;
        }

        $servers = explode(',', $result['server']);

        return $servers;
    }

    public function Create() {
        $data = [];
        $data['facility'] = $this->resource('location');
        $data['plan'] = $this->resource('type');
        $data['operating_system'] = $this->resource('os');
        $data['hostname'] = $this->account_details['domain'];

        $sshKey = $this->resource('sshkey');



        if($sshKey){


            $ssh_keys = [];
            $sshkeys = HBLoader::LoadComponent('SSHKeys');
            foreach ($sshKey as $ssh){
                $key = $sshkeys::key($ssh);
                if (!empty($key['key'])){
                    $ssh_keys[]   = $key['key'];
                }
            }


            if (empty($ssh_keys))
            {
                $this->addError('Invalid or empty SSH Key');
                return false;
            }


            $keys = $this->getSshKeyId($ssh_keys, $data['hostname']);



            if($keys){
                foreach($keys AS $key){
                    $data['project_ssh_keys'][] = $key;
                }

            }
            else
                return false;
        }


        $endpoint = 'projects/'.$this->getProjectId().'/devices';

        $hosting = $this->call($endpoint, 'POST', $data);

        if($hosting AND is_array($hosting)) {
            if(!$hosting['id']){
                $this->addError('Wrong or empty device ID');
                return false;
            }

            $this->details['device_id']['value'] = $hosting['id'];

            return true;
        }

        return false;

    }

    public function getSshKeyId($ssh_keys, $label){
        $endpoint = 'projects/' .$this->getProjectId().'/ssh-keys';

        $keys = $this->call($endpoint, 'GET');

        if(!$keys OR !is_array($keys)){
            return false;

        }

        $exists = [];
        $toAdd= [];

        foreach($ssh_keys AS $ssh_key){
            $is = 0;
            foreach($keys['ssh_keys'] AS $key){
                if($key['key']==$ssh_key){
                    $exists[] =  $key['id'];
                    $is = 1;
                }
            }

            if($is==0 AND !in_array($ssh_key, $toAdd)){
                $toAdd[] = $ssh_key;
            }
        }

        //HBDebug::debug('HB < packet-ssh ',[$exists]);

        $newIds= [];

        if(!empty($toAdd)) {
            $newIds =  $this->addSshKey($toAdd, $label);
        }

        return array_merge($exists, $newIds);

    }

    public function addSshKey($sshKeys, $label){

        if($this->getProjectId()) {
            $endpoint = 'projects/' .$this->getProjectId().'/ssh-keys';

            $newIds = [];

            foreach($sshKeys AS $sshKey){
                $data = [
                    'label' => $label.'_'.time(),
                    'key'   => $sshKey
                ];


                $response = $this->call($endpoint, 'POST', $data);

                if(!$response OR $response['errors'] OR !is_array($response))
                    return false;

                $newIds[] = $response['id'];
            }
            return $newIds;

        }


        $this->addError('Project Id is not avalible ');
        return false;

    }



    public function Suspend(){


        $endpoint = 'devices/'.$this->account_details['extra_details']['device_id'].'/actions?type=power_off';
        HBDebug::debug('HB < packet-reboot ', [$endpoint]);

        $response = $this->call($endpoint, 'POST');

        if(!$response OR !$response['errors']){
            return true;
        }

        return false;
    }

    public function Unsuspend(){
        $endpoint = 'devices/'.$this->account_details['extra_details']['device_id'].'/actions?type=power_on';

        $response = $this->call($endpoint, 'POST');



        if(!$response OR !$response['errors']){
            return true;
        }

        return false;
    }

    public function Reboot(){
        $endpoint = 'devices/'.$this->account_details['extra_details']['device_id'].'/actions?type=reboot';



        $response = $this->call($endpoint, 'POST');

        if(!$response OR !$response['errors']){
            return true;
        }

        return false;
    }


    public function Terminate(){

        $endpoint = 'devices/'.$this->account_details['extra_details']['device_id'];

        $response = $this->call($endpoint, 'DELETE');

        if(!$response OR !$response['errors']){
            return true;
        }

        return false;
    }

    public function getServerId() {
        $id = $this->details['device_id']['value'];

        return $id;
    }

    public function getServerDetails(){

        $endpoint = 'devices/'.$this->getServerId();

        $details = $this->call($endpoint, 'GET');

        return $details;
    }

    public function getUsageData($params){

        HBDebug::debug('HB < packet-data2 ', [strtotime($params['start']), strtotime($params['stop'])]);
        $query = $this->db->prepare("SELECT hba.`extra_details`, hbs.username FROM hb_accounts AS hba LEFT JOIN hb_servers AS hbs ON (hba.server_id = hbs.id)  WHERE hba.`id` = :account_id");
        $query->execute(array('account_id' => $params['id']));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $query->closeCursor();

        $details = unserialize($result['extra_details']);
        $this->setApiKey($result['username']);



        $endpoint = 'devices/'.$details['device_id'].'/bandwidth?from='.strtotime($params['start'].'&until='.strtotime($params['stop']));

        $data = $this->call($endpoint, 'GET');

        $data = $data['bandwidth'];
        HBDebug::debug('HB < packet-data3 ', [$data]);
        $result = [];

        foreach($data[0]['datapoints'] AS $in){

            $result[$in[1]]= [
                'time'  => $in[1],
                'netin' => $in[0]?round($in[0]/128, 2):0,
            ];
        }
        foreach($data[1]['datapoints'] AS $out){
            $result[$out[1]]['netout'] = $out[0]?round($out[0]/128, 2):0;
        }

        echo json_encode($result);

    }

    public function Rescue(){
        $endpoint = 'devices/'.$this->account_details['extra_details']['device_id'].'/actions?type=rescue';

        $response = $this->call($endpoint, 'POST');

        if(!$response OR !$response['errors']){
            return true;
        }

        return false;
    }

    public function reinstall(){
        $all_systems =$this->getAvailableOS(true);

        $server_detail = $this->getServerDetails();

        $plan = $server_detail['plan']['name'];
        $systems = [];

        foreach($all_systems AS $system){
            foreach($system['provisionable_on'] AS $type){
               if($type == $plan){
                    $systems[] = [
                        'id'    => $system['slug'],
                        'name'  => $system['name']
                    ];
               }

            }
        }
        return $systems;
    }

    public function reinstallQuery($os_id){
        $endpoint = 'devices/'.$this->account_details['extra_details']['device_id'].'/actions?type=reinstall';

        $data = [];
        $data['operating_system'] = $os_id;
        $response = $this->call($endpoint, 'POST', $data);

        if(!$response){
            return true;

        }

        return false;


    }





}