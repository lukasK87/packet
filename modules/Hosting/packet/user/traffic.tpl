{include file="`$onappdir`header.cloud.tpl"}
    <link rel="stylesheet" type="text/css" href="{$system_url}includes/modules/Hosting/{$modulename}/user/css/style.css">
    <div class="header-bar">
        <h3 class="{$vpsdo} hasicon">{if $lang.traffic}{$lang.traffic}{else}Traffic{/if}</h3>
        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="{$system_url}includes/modules/Hosting/{$modulename}/user/js/chart.bundle.min.js"></script>

    <div class="content-bar" style="background:#f3f3f3; padding-top: 5%;">
        <div style="margin:0px auto;">
            <div >


                <div id="reportrange" >
                    <i class="fa fa-calendar"></i>&nbsp;
                    <input type="text" name="datepicker" readonly /> <i class="fa fa-caret-down"></i>
                </div>

                <div class="graph_usage" data-type="netin,netout" >
                    <canvas id="traffic-graph"></canvas>
                </div>
            </div>
        </div>

    </div>


{literal}
    <style>
        .graph_usage {
            text-align: center;
            background: #fff;
        }
    </style>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <script>


        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('input[name="datepicker"]').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

                loadgraphs(start, end);

            }

            $('input[name="datepicker"]').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });

        window.chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };

        pxe_sid = '{/literal}{$service.id}{literal}';
        global_graph_config = {

            type: 'line',
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Usage'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        type: 'time',
                        time: {
                            tooltipFormat: 'll h:mm:ss a'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Time'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero:true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Bytes/s'
                        }
                    }]
                }
            }
        };


        function loadgraphs(start, stop){

            var cf = $('select[name=cf]').val();
            $('.content-bar').addLoader();

            $.post('{/literal}{$url}{literal}',{action: 'usage', id: pxe_sid, cf: cf, rrdata:true, start: start.format('MMMM D, YYYY'), stop: stop.format('MMMM D, YYYY')},function(data) {
                data = data.replace('<!-- {"ERROR":[],"INFO":[],"STACK":0} -->', '');
                console.log(typeof(data));
                console.log(data);


                data = JSON.parse(data);
                data = Object.values(data);
                console.log(typeof(data));
                if(typeof(data)!=='object')
                    return;

                $('#preloader').remove();
                $('#preloader').hide();
                if(!data.length || !data.length){
                    alert('Connection error');
                    return ;

                }


                window.netgraph =  null;

                var preloader = $('#preloader');

                $('#preloader').remove();
                $('#preloader').hide();


                var graphdata={
                    netin: [],
                    netout: []
                };


                for(var i=0;i<data.length;i++) {
                    for (var property in graphdata) {
                        if (graphdata.hasOwnProperty(property)) {
                            graphdata[property].push({
                                t: data[i].time * 1000,
                                y: data[i][property]
                            });
                        }
                    }
                }




                var netsettings =
                    $.extend({data: {
                            datasets: [{
                                backgroundColor: window.chartColors.purple,
                                borderColor: window.chartColors.purple,
                                fill: false,
                                data: graphdata.netin,
                                label: "Net In"
                            },{
                                backgroundColor: window.chartColors.grey,
                                borderColor: window.chartColors.grey,
                                fill: false,
                                data: graphdata.netout,
                                label: "Net Out"
                            }
                            ]
                        }}, global_graph_config);


                netsettings.options.scales.yAxes[0].scaleLabel.labelString = 'Kb';
                window.netgraph = new Chart(document.getElementById("traffic-graph").getContext("2d"),
                    netsettings
                );

                //console.log(graphdata);
            }).fail(function(error) {
                alert('Connection error');
                $('#preloader').remove();
                $('#preloader').hide(); }
                );
        };


    </script>
{/literal}


{include file="`$onappdir`footer.cloud.tpl"}


