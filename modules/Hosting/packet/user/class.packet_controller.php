<?php

/**
 * Class hetznercloud_controller
 * User controller
 * @see https://dev.hostbillapp.com/dev-kit/advanced-topics/hostbill-controllers/
 * @author HostBill <cs@hostbillapp.com>
 */
class packet_controller extends HBController {

    /**
     * Related module object (hetznercloud)
     * @var hetznercloud $module
     */
    var $module;


    /**
     * User Authorization object
     * Use $this->authorization->get_login_status() to check if user is logged in or not.
     * Use $this->authorization->get_id() - to get id of logged in customer
     * @var UserAuthorization $authorization
     */
    var $authorization;

    /**
     * Template object (subclass of Smarty).
     * Use it to assign variables to template
     * @var Smarty $template
     */
    var $template;


    protected $clientareapath;
    protected $section;
    protected $prefix = '_';

    public function accountdetails($params) {

        $this->template->assign('commontpl', MAINDIR . 'templates' . DS . 'common' . DS . 'cloudhosting' . DS);
        $this->template->assign('commondir', MAINDIR . 'templates' . DS . 'common' . DS . 'cloudhosting' . DS);

        $this->clientareapath = APPDIR_MODULES
            . $this->module->getModuleType()
            . DS . strtolower($this->module->getModuleName())
            . DS . 'user' . DS;
        $this->template->assign('onappdir', $this->clientareapath);
        Engine::singleton()->getObject('language')->addTranslation('onapp');
        $extra = $params['account'];



        if ($extra['status'] == 'Active' && $params['vpsdo'] != 'billing' && $params['vpsdo'] != 'networking' && $params['vpsdo'] != 'traffic' && $params['vpsdo'] != 'reinstall' ) {

            $dropletId = $this->module->getServerId();

            if (is_callable(array($this, 'singlevm_' . $params['vpsdo']))){
                $this->{'singlevm_' . $params['vpsdo']}($params);
            }elseif (!empty($dropletId)){
                $params['uuid'] = $dropletId;
                $this->singlevm_default($params);
            }


            $this->template->assign('s_vm', $this->session2vm());
            $info = $this->module->getServerDetails();
            if(in_array($info['state'], ['queued', 'provisioning']))
                $this->template->assign('o_sections',[
                    'o_rebuild' => false,
                    'o_reboot' => false,
                    'o_startstop' => false,
                ]);
            elseif($info['state']=='inactive')
                $this->template->assign('o_sections',[
                    'o_rebuild' => false,
                    'o_reboot' => false,
                    'o_startstop' => true,
                ]);
            else
                $this->template->assign('o_sections',[
                    'o_rebuild' => true,
                    'o_reboot' => true,
                    'o_startstop' => true,
                ]);

        }
        elseif ($params['vpsdo'] == 'reinstall' && $extra['status'] == 'Active') {

            $this->reinstall($params);
        }
        elseif ($params['vpsdo'] == 'logs' && $extra['status'] == 'Active') {

            $this->logs($params);
        }
        elseif ($params['vpsdo'] == 'networking' && $extra['status'] == 'Active') {

            $this->networking($params);
        }
        elseif ($params['vpsdo'] == 'traffic' && $extra['status'] == 'Active') {

            $this->traffic($params);
        }
        elseif ($extra['status'] == 'Active' && $params['vpsdo'] == 'billing') {
            $this->billing($params);
        }
        elseif($extra['status'] == 'Active' && $params['vpsdo'] == 'usage'){
            $this->usage($params);
        }


        $this->template->assign('vpsdo', isset($params['vpsdo']) ? Registrator::paranoid($params['vpsdo']) : false);
        $this->template->assign('vpsid', isset($params['vpsid']) ? Registrator::paranoid($params['vpsid']) : false);

        $this->section = empty($this->section) ? 'vmdetails' : $this->section;
        $this->template->showtpl = $this->clientareapath . $this->section;
        $this->template->assign('vmsection', $this->section);
        $this->template->assign('modulename', $this->module->getModuleName());
    }

    protected function billing(&$params) {
        $params['vpsdo'] = 'billing';
        $this->section = 'billing';
    }

    protected function networking($params){

        $server = $this->module->getServerDetails();



        $ip_addresses= [];

        foreach($server['ip_addresses'] AS $address){


            $ip_addresses[] = [
                'address'   => $address['address'],
                'type'      => ($address['public']? 'Public' : 'Private').' IPv'.$address['address_family'],
                'network'   => $address['network'].'/'.$address['cidr'],
                'gateway'   => $address['gateway']
            ];
        }


        $url = Utilities::url('?cmd=module&module='.$this->module->getModuleId());

        $this->template->assign('url', $url);
        $this->template->assign('addresses', $ip_addresses);
        $params['vpsdo'] = 'networking';
        $this->section = 'networking';

    }

    protected function traffic($params){


        $url = Utilities::url('?cmd=module&module='.$this->module->getModuleId());


        $this->template->assign('url', $url);
        $params['vpsdo'] = 'traffic';
        $this->section = 'traffic';

    }


    protected function reinstall($params){


        $security_token = Tokenizer::getSalt();
        $os = $this->module->reinstall();



        if($params['system'] && !$this->module->reinstallQuery($params['system'])) {
            $this->module->reinstallQuery($params['system']);
            $this->template->assign('system', $params['system']);
        }
        $this->template->assign('token', $security_token);
        $this->template->assign('os', $os);
        $params['vpsdo'] = 'reinstall';
        $this->section = 'reinstall';

    }

    protected function singlevm_rescue($params) {

        if ($params['token_valid'])
            $this->module->Rescue();
        Utilities::redirect('?cmd=clientarea&action=services&service=' . $params['service']);
    }

    protected function singlevm_shutdown($params) {
        if ($params['token_valid'])
            $this->module->Suspend();
        Utilities::redirect('?cmd=clientarea&action=services&service=' . $params['service']);
    }

    protected function singlevm_startup($params) {
        if ($params['token_valid'])
            $this->module->Unsuspend();
        Utilities::redirect('?cmd=clientarea&action=services&service=' . $params['service']);
    }

    protected function singlevm_reboot($params) {
        if ($params['token_valid'])
            $this->module->Reboot();
        Utilities::redirect('?cmd=clientarea&action=services&service=' . $params['service']);
    }

    protected function singlevm_default($params) {
        $this->section = 'vmdetails';
        $vpsinfo = $this->session2vm();


        if (!is_array($vpsinfo))
            $vpsinfo = array();

        $vpsinfo = array_merge($vpsinfo, $this->module->getServerDetails());

        if ($vpsinfo == false && !$this->module->getServerId())
            Utilities::redirect('?cmd=clientarea&action=services&service=' . $params['service']);
        $this->vm2session($vpsinfo);

        $this->template->assign('vpsdo', 'details');
        if ($params['vpsdo'] == 'vmactions') {
            $this->section = 'vmactions';
        }

        //var_dump($vpsinfo);
        //die();

        $this->template->assign('vpsdetails', $vpsinfo);
        $this->template->assign('VMDetails', $vpsinfo);

        if (Controller::isAjax() && isset($params['status'])) {
            $this->template->assign('service_id', $params['account']['id']);
            $this->template->assign('vpsid', $params['vpsid']);
            $this->template->display(APPDIR_MODULES
                . $this->module->getModuleType()
                . DS . strtolower($this->module->getModuleName())
                . DS . 'user'
                . DS . 'ajax.vmstatus.tpl');
            die();
        }
    }

    protected function vm2session($vm) {
        $vmodl = HBConfig::getSetting('_packet');

        if (!is_array($vmodl))
            $vmodl = array();
        $vm = array_merge($vmodl, $vm, array(
            'time' => time()
        ));
        HBConfig::storeSetting($vm, '_packet');
    }

    protected function session2vm() {
        return HBConfig::getSetting('_packet');
    }


    public function usage($params){

        return $this->module->getUsageData($params);
    }
}