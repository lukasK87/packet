{include file="`$onappdir`header.cloud.tpl"}
<div>
    {if $system}
        <span>New OS is installing</span>
    {else}
        <form action="?cmd=clientarea&action=services&service={$service.id}&vpsdo=reinstall&vpsid={$vpsid}" method="POST">
            <span>Please select an operating system</span>
            <br />
            <select name="system">

                {foreach from =$os  item=system}

                    <option value="{$system.id}">{$system.name}</option>
                {/foreach}
            </select>
            <br />
            <input type="hidden" name="security_token" value="{$token}"/>
            <input type="submit" value="Reinstall" />
        </form>
    {/if}
</div>

{include file="`$onappdir`footer.cloud.tpl"}