<link media="all" type="text/css" rel="stylesheet" href="templates/common/cloudhosting/styles.css" />
<script type="text/javascript" src="templates/common/cloudhosting/js/scripts.js"></script>
<div class="cloud">
{if $widget.appendtpl }
    {include file=$widget.appendtpl}
{/if}
    {cloudservices
    section='header'
    include="../common/cloudhosting/tpl/header.tpl"}
<div id="nav-onapp">
    <h1 class="left os-logo {if $s_vm}{$s_vm.distro}{/if}">{$vpsdetails.hostname}</h1>
    <ul class="level-1">
        <li class="{if $vmsection=='vmdetails'}current-menu-item{/if}"><a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=vmdetails&vpsid={$vpsid}"><span class="list-servers">{$lang.Overview}</span></a></li>
        <li class="{if $vmsection=='networking'}current-menu-item{/if}"><a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=networking&vpsid={$vpsid}"><span class="addserver">{if $lang.networking}{$lang.networking}{else}Networking{/if}</span></a></li>
        <li class="{if $vmsection=='traffic'}current-menu-item{/if}"><a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=traffic&vpsid={$vpsid}"><span class="addserver">{if $lang.traffic}{$lang.traffic}{else}Traffic{/if}</span></a></li>
        <li class="{if $vmsection=='billing'}current-menu-item{/if}"><a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=billing&vpsid={$vpsid}"><span class="addserver">{$lang.Billing}</span></a></li>
        <li class="{if $vmsection=='reinstall'}current-menu-item{/if}"><a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=reinstall&vpsid={$vpsid}"><span class="addserver">{if $lang.reinstall}{$lang.reinstall}{else}Reinstall{/if}</span></a></li>
    </ul>
    <div class="clear"></div>
</div>
    {/cloudservices}
<div class="clear"></div>
<div id="content-cloud">
