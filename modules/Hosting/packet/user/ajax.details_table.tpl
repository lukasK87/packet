{cloudservices section='details'
include="../common/cloudhosting/tpl/details.tpl"
}
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td width="50%" style="padding-right:10px;">
            <table cellpadding="0" cellspacing="0" width="100%" class="ttable">
                <tr>
                    <td width="120">
                        <b>{$lang.status}</b>
                    </td>
                    <td id="status-button" style="padding:8px 5px 9px;">
                            {if $VMDetails.state =='active'}
                                <a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=shutdown&vpsid={$VMDetails.id}&security_token={$security_token}" onclick="return
                                powerchange(this, 'Are you sure you want to Power OFF this VM?');" class="state_switch on" >{$lang.On}</a>
                            {else}
                                <a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=startup&vpsid={$VMDetails.id}&security_token={$security_token}" onclick="return
    powerchange(this, 'Are you sure you want to Power ON this VM?');" class="state_switch off" >{$lang.Off}</a>
                            {/if}

                    </td>
                </tr>
                <tr>
                    <td ><b>{$lang.hostname}</b> </td>
                    <td >{$VMDetails.hostname}</td>
                </tr>
                <tr>
                    <td ><b>{$lang.ipadd}</b> </td>
                    <td>
                        {foreach from=$VMDetails.ip_addresses item=address }
                            {$address.address}<br />
                        {/foreach}
                    </td>
                </tr>

                {if $VMDetails.root_password}
                    <tr>
                        <td ><b>{$lang.password}</b> </td>
                        <td>
                            <a href="#" onclick="$(this).hide();$('#rootpass_div').show();return false;" style="color:red">{$lang.show}</a>
                            <div id="rootpass_div" style="display:none; background-color: #fffbc8; width:50%; border-radius:7px;">

                                <span id="rootpss" >{$VMDetails.root_password} </span><br /><span style="font-size:60%; vertical-align: super;">SSH root password (valid for 24h) </span>
                            </div>
                        </td>
                    </tr>
                {/if}
            </table>
        </td>
    </tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td width="50%" style="padding-left:10px;">
            <table  cellpadding="0" cellspacing="0" width="100%" class="ttable">
                <tr>
                    <td >
                        <b>{$lang.Location}</b>
                    </td>
                    <td >
                        {$VMDetails.facility.name}
                    </td>
                </tr>
                <tr >
                    <td  >
                        <b>{$lang.cpucores}</b>
                    </td>
                    <td valign="top" style="">
                        {foreach from=$VMDetails.plan.specs.cpus item=cpu }
                            {$cpu.count} x {$cpu.type} <br />
                        {/foreach}
                    </td>
                </tr>
                <tr>
                    <td >
                        <b>{$lang.memory}</b>
                    </td>
                    <td valign="top" style="">
                        {$VMDetails.plan.specs.memory.total} GB {if $lang.ram}{$lang.ram}{else}RAM{/if}
                    </td>
                </tr>
                <tr>
                    <td class="lst">
                        <b>{$lang.storage}</b>
                    </td>
                    <td valign="top" style="">
                      {foreach from=$VMDetails.plan.specs.drives item=disk }
                        {$disk.count} x {$disk.size}  {$disk.type}
                      {/foreach}
                    </td>
                </tr>
                <tr>
                    <td class="lst">
                        <b>{if $lang.nic}{$lang.nic}{else}NIC{/if}</b>
                    </td>
                    <td valign="top" style="">
                        {foreach from=$VMDetails.plan.specs.nics item=nic }
                            {$nic.count} x {$nic.type}
                        {/foreach}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
{/cloudservices}