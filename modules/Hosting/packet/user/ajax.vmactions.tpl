{cloudservices section='actions'
include="../common/cloudhosting/tpl/actions.tpl"
}

<div class="right">
    <div id="lock" {if $VMDetails.locked}style="display:block"{/if}>
        <img src="includes/modules/Hosting/amazon_lightsail/templates/clientarea/images/ajax-loader.gif" alt=""> {$lang.server_performing_task}
        <a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=vmdetails&vpsid={$vpsid}">
            <b>{$lang.refresh}</b>
        </a>
    </div>
    <ul id="vm-menu" class="right">
        {if $VMDetails.state =='active'}
            <input id="state" data-state="running" type="hidden" value="started">
            {if $o_sections.o_reboot}
                <li>
                    <a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=reboot&vpsid={$vpsid}&security_token={$security_token}" onclick="return confirm('{$lang.sure_to_reboot}?');">
                        <img alt="Reboot" src="templates/common/cloudhosting/images/icons/24_arrow-circle.png"><br>{$lang.reboot}
                    </a>
                </li>
            {/if}
            <li>
                <a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=shutdown&vpsid={$vpsid}&security_token={$security_token}" onclick="return confirm('Are you sure you wish to shutdown this VM?');">
                    <img alt="Shutdown" src="templates/common/cloudhosting/images/icons/poweroff.png"><br>{$lang.Shutdown}
                </a>
            </li>
        {else}
            <input id="state" data-state="stopped" type="hidden" value="stopped">
            <li>
                <a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=startup&vpsid={$vpsid}&security_token={$security_token}" >
                    <img alt="Shutdown" src="templates/common/cloudhosting/images/icons/poweroff.png"><br>{$lang.Startup}
                </a>
            </li>
        {/if}
        <li>
            <a href="?cmd=clientarea&action=services&service={$service.id}&vpsdo=rescue&vpsid={$vpsid}&security_token={$security_token}" onclick="return confirm('Are you sure you wish to run in rescue mode?');">
                <img alt="Reinstall" src="templates/common/cloudhosting/images/icons/24_terminal.png"><br>{$lang.rescue}
            </a>
        </li>

        {foreach from=$widgets item=widg key=wkey}
            <li class="widget">
                <a href="{$ca_url}clientarea/services/{$service.slug}/{$service.id}/&vpsid={$vpsid}&vpsdo=vmdetails&widget={$widg.name}{if $widg.id}&wid={$widg.id}{/if}">
                    <img src="{$widg.config.bigimg}" alt=""><br/>{if $lang[$widg.name]}{$lang[$widg.name]}{elseif $widg.fullname}{$widg.fullname}{else}{$widg.name}{/if}
                </a>
            </li>
        {/foreach}
    </ul>
</div>
{/cloudservices}
<div class="clear"></div>
{include file="`$onappdir`ajax.details_table.tpl"}

{if !$VMDetails.locked || $VMDetails.state == 'provisioning' || $VMDetails.state == 'queued'}
    {literal}
        <script type="text/javascript">
            var wx = setTimeout(function() {
                $.post('{/literal}?cmd=clientarea&action=services&service={$service.id}&vpsid={$vpsid}{literal}', {vpsdo: 'vmactions'}, function(data) {
                    var r = parse_response(data);
                    if (r)
                        $('#lockable-vm-menu').html(r);
                });
            }, 4000);
        </script>
    {/literal}
{/if}