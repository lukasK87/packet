{include file="`$onappdir`header.cloud.tpl"}
{if $addresses}
    <link rel="stylesheet" type="text/css" href="{$system_url}includes/modules/Hosting/{$modulename}/user/css/style.css">
    <div class="header-bar">
        <h3 class="{$vpsdo} hasicon">{if $lang.networnikg}{$lang.networnikg}{else}Networking{/if}</h3>
        <div class="clear"></div>
    </div>
    <div class="content-bar ">
        <table id="protocol-table" cellpadding="0" cellspacing="0" class="ttable" width="100%" style="margin-top:0px">
            <thead>
                <tr>
                    <th>
                        IP ADDRESS
                    </th>
                    <th>
                        Type
                    </th>
                    <th>
                        NETWORK
                    </th>
                    <th>
                        GATEWAY
                    </th>


                </tr>
            </thead>
            <tbody id="tbody-port">
                {foreach from=$addresses item=address }
                    <tr>
                        <td>{$address.address}</td>
                        <td>{$address.type}</td>
                        <td>{$address.network}</td>
                        <td>{$address.gateway}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>


    </div>


{/if}
{include file="`$onappdir`footer.cloud.tpl"}

