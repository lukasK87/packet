var i = 0;
var ports_value = new Array();
ports_value.push({
    fromPort: 1,
    toPort: 65535,
    protocol: "tcp",
    name: 'All TCP'
});
ports_value.push({
    fromPort: 1,
    toPort: 65535,
    protocol: "udp",
    name: 'All UDP'
});
ports_value.push({
    fromPort: 22,
    toPort: 22,
    protocol: "tcp",
    name: 'SSH'
});
ports_value.push({
    fromPort: 3389,
    toPort: 3389,
    protocol: "tcp",
    name: 'RDP'
});
ports_value.push({
    fromPort: 3306,
    toPort: 3306,
    protocol: "tcp",
    name: 'MySQL/Aurora'
});
ports_value.push({
    fromPort: 5432,
    toPort: 5432,
    protocol: "tcp",
    name: 'PostgreSQL'
});
ports_value.push({
    fromPort: 1521,
    toPort: 1521,
    protocol: "tcp",
    name: 'Oracle-RDS'
});
ports_value.push({
    fromPort: 80,
    toPort: 80,
    protocol: "tcp",
    name: 'HTTP'
});
ports_value.push({
    fromPort: 443,
    toPort: 443,
    protocol: "tcp",
    name: 'HTTPS'
});
ports_value.push({
    fromPort: 53,
    toPort: 53,
    protocol: "udp",
    name: 'DNS (UDP)'
});
ports_value.push({
    fromPort: 53,
    toPort: 53,
    protocol: "tcp",
    name: 'DNS (TCP)'
});
ports_value.push({
    fromPort: 1,
    toPort: 65535,
    protocol: [{1:'tcp', 2: 'udp'}],
    name: 'Custom'
});
var rules_id = new Array();
var new_rules = new Array();


$(function () {
    $('#save-ports').hide();
    $('#cancel-ports').hide();
    $('.delete_port').hide();
    $('#new-port-div').hide();
    $('#refresh').hide();

    $('#add-ports').click(function () {
        $('#add-ports').hide();
        $('#remove-ports').hide();
        $('#save-ports').show();
        $('#cancel-ports').show();
        $('#save-ports').data('action', 'add');
        $('#cancel-ports').data('action', 'add');
        $('#tbody-port').append(field(i));
        addValue(i);
        $('#new-port-div').show();
        i++;
    });
    $('#remove-ports').click(function () {
        $('.delete_port').show();
        $('#add-ports').hide();
        $('#remove-ports').hide();
        $('#save-ports').show();
        $('#cancel-ports').show();
        $('#save-ports').data('action', 'remove');
        $('#cancel-ports').data('action', 'remove');
    });

});

function remove(id) {
    $('.port-'+id).hide();
    rules_id.push(id);
}

function savechange() {
    var action = $('#save-ports').data('action');
    var service_id = $('#service_id').val();
    var vpsdo = $('#service_section').val();
    $('#save-ports').hide();
    $('#cancel-ports').hide();
    $('#refresh').show();
    $('#spin').animateRotate(3600, {
        duration: 10000,
        easing: 'linear',
        complete: function () {},
        step: function (now) {$(this).css({ transform: 'rotate('+now+'deg)'});}
    });
    $('.select-port').each(function () {
        getval(this, false);
    });
    if (action == 'remove'){
        $.post('?cmd=clientarea&action=services&service='+service_id+'&vpsdo='+vpsdo,
            {
                rules_id: rules_id,
                event: 'remove'
            }, function(data) {
                location.reload();
            });
    }
    if (action == 'add'){
        $.post('?cmd=clientarea&action=services&service='+service_id+'&vpsdo='+vpsdo,
            {
                rules: new_rules,
                event: 'add'
            }, function(data) {
                location.reload();
            });
    }
}

function cancelchange() {
    var action = $('#cancel-ports').data('action');
    if (action == 'remove'){
        $('.port').show();
        $('.delete_port').hide();
        rules_id = new Array();
    }
    if (action == 'add'){
        $('.new-port').hide();
        $('#new-port-div').hide();
        i = 0;
        new_rules = new Array();
    }

    $('#save-ports').hide();
    $('#cancel-ports').hide();
    $('#add-ports').show();
    $('#remove-ports').show();
}

function addnewportfield() {
    $('#tbody-port').append(field(i));
    addValue(i);
    i++;
}

function getval(sel, add) {
    var tr = $(sel).parent().closest('tr');
    var field_id = tr.data('id');
    if (ports_value[sel.value].name != 'Custom'){
        tr.children('td').eq(1).html(ports_value[sel.value].protocol.toUpperCase());
        if (ports_value[sel.value].fromPort == ports_value[sel.value].toPort){
            tr.children('td').eq(2).html(ports_value[sel.value].fromPort);
        }else{
            tr.children('td').eq(2).html(ports_value[sel.value].fromPort+'-'+ports_value[sel.value].toPort);
        }
        addValue(field_id, sel);
    }else{
        if (add){
            tr.children('td').eq(1).html('<select style="max-width: 80px;" onchange="changeCustom(this)"><option value="tcp">TCP</option><option value="udp">UDP</option><option value="icmp">ICMP</option> </select>');
            tr.children('td').eq(2).html('<input style="max-width: 60px;" onchange="changeCustom(this)" type="number" name="custom" min="0" max="65535" value="100"> - <input style="max-width: 60px;" onchange="changeCustom(this)" type="number" name="custom" min="0" max="65535" value="100">');
        }
        changeCustom(sel);
    }
}

function addValue(id, sel) {
    if (typeof sel === 'undefined' || sel === null){
        new_rules[id] = {
            protocol: ports_value[0].protocol,
            port_start: parseInt(ports_value[0].fromPort),
            port_end: parseInt(ports_value[0].toPort),
            comment: ports_value[0].name
        };
    }else{
        new_rules[id] = {
            protocol: ports_value[sel.value].protocol,
            port_start: parseInt(ports_value[sel.value].fromPort),
            port_end: parseInt(ports_value[sel.value].toPort),
            comment: ports_value[sel.value].name
        };
    }
    new_rules[id]['address_start'] = $('#address_start_'+id).val();
    new_rules[id]['address_end'] = $('#address_end_'+id).val();
    new_rules[id]['type'] = $('#type_'+id).val();
    new_rules[id]['action'] = $('#action_'+id).val();
}

function changeCustom(sel) {
    var tr = $(sel).parent().closest('tr'),
        field_id = tr.data('id'),
        comment = tr.children('td').eq(0).children('select').val(),
        protocol = tr.children('td').eq(1).children('select').val(),
        port1 = tr.children('td').eq(2).children('input').eq(0).val(),
        port2 = tr.children('td').eq(2).children('input').eq(1).val();
    new_rules[field_id] = {
        protocol: protocol,
        port_start: parseInt(port1),
        port_end: parseInt(port2),
        comment: ports_value[comment].name
    };
    new_rules[field_id]['address_start'] = $('#address_start_'+field_id).val();
    new_rules[field_id]['address_end'] = $('#address_end_'+field_id).val();
    new_rules[field_id]['type'] = $('#type_'+field_id).val();
    new_rules[field_id]['action'] = $('#action_'+field_id).val();
}

function field(id) {
    var field = '<tr id="new-port-'+id+'" class="new-port" data-id="'+id+'"><td><select style="max-width: 130px;" class="select-port" onchange="getval(this, true)">';
    $.each(ports_value, function (index, value) {
        field += '<option value="'+index+'">'+value.name+'</option>';
    });
    field += '</select></td><td>'+ports_value[0].protocol.toUpperCase()+'</td><td>'+ports_value[0].fromPort+'-'+ports_value[0].toPort+'</td>';
    field += '<td><input id="address_start_'+id+'"  type="text" style="max-width: 110px;"> - <input id="address_end_'+id+'" name="address_end" type="text" style="max-width: 110px;"></td>';
    field += '<td><select id="type_'+id+'" name="type" style="max-width: 110px;"><option value="destination">Destination</option><option value="source">Source</option><option value="both">Both</option></select></td>';
    field += '<td><select id="action_'+id+'" name="action" style="max-width: 80px;"><option value="accept">Accept</option><option value="drop">Drop</option></select></td></tr>';
    return $(field);
}

$.fn.animateRotate = function(angle, duration, easing, complete) {
    var args = $.speed(duration, easing, complete);
    var step = args.step;
    return this.each(function(i, e) {
        args.complete = $.proxy(args.complete, e);
        args.step = function(now) {
            $.style(e, 'transform', 'rotate(' + now + 'deg)');
            if (step) return step.apply(e, arguments);
        };

        $({deg: 0}).animate({deg: angle}, args);
    });
};