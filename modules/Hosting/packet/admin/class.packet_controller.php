<?php

/**
 * Class _controller
 * Admin controller
 * @see https://dev.hostbillapp.com/dev-kit/advanced-topics/hostbill-controllers/
 * @author HostBill <cs@hostbillapp.com>
 */
class packet_controller extends HBController {

    /**
     * Related module object ()
     * @var  $module
     */
    var $module;

    /**
     * Admin authorization object
     * Use $this->authorization->get_id() - to get id of logged in staff member
     * @var AdminAuthorization
     */
    var $authorization;

    /**
     * Template object (subclass of Smarty).
     * Use it to assign variables to template
     * @var Smarty $template
     */
    var $template;

    protected function load_form($params) {

        if ($params['id'] == 'new') {
            Engine::addError("Please save your product first");
            return;
        }

        $f = HBLoader::LoadModel('ConfigFields');
        Engine::singleton()->getObject('language')->addTranslation('configfields');

        $var = array(
            'type' => 'select',
            'category' => 'software',
            'premade' => '1',
            'product_id' => $params['id'],
            'options' => ConfigOption::OPTION_SHOWCART | ConfigOption::OPTION_REQUIRED,
            'items' => array(),
            'variable' => $params['variableid']
        );
        if ($params['variableid'] == 'type') {
            $var['name'] = 'Server Type';
            $objlist = $this->module->getServerTypes();
        } elseif ($params['variableid'] == 'image') {
            $var['name'] = 'Image';
            $objlist = $this->module->getImages();
        } elseif ($params['variableid'] == 'location') {
            $var['name'] = 'Location';
            $objlist = $this->module->getLocations();
        } elseif($params['variableid'] == 'sshkey') {
            $var['name'] = 'SSH Key';
            $var['type'] = 'sshkeyselect';
            $var['options'] = ConfigOption::OPTION_SHOWCART;
            $var['items'][] = array('variable_id' => 'sshkey', 'name' => 'SSH Key');
        } elseif ($params['variableid'] == 'volume') {
            $var['name'] = 'Volume';
            $var['type'] = 'radio';
            $var['options'] = ConfigOption::OPTION_SHOWCART;
            $var['items'][] = array('variable_id' => 'on', 'name' => 'On');
            $var['items'][] = array('variable_id' => 'off', 'name' => 'Off');
        } elseif ($params['variableid'] == 'volume_size') {
            $var['name'] = 'Volume Size';
            $var['type'] = 'input';
            $var['options'] = ConfigOption::OPTION_SHOWCART;
            $var['description'] = 'Size of the volume in GB';
            $var['items'][] = array('variable_id' => $params['variableid'], 'name' => 'Volume Size');
        } elseif ($params['variableid'] == 'limitsnapshots') {
            $var['type'] = 'sliderinput';
            $var['name'] = 'Limit Snapshots';
            $var['options'] = ConfigOption::OPTION_SHOWCART;
            $var['config'] = array(
                'maxvalue' => 30,
                'minvalue' => 1,
                'step' => 1,
                'initialval' => 1
            );
            $var['items'][] = array('variable_id' => 'limitsnapshots', 'name' => 'Limit Snapshots');
        }
        if (isset($objlist)) {
            foreach ($objlist as $obj) {
                if ($var['variable'] == 'type'){
                    $var['items'][] = array(
                        'variable_id' => $obj['name'],
                        'name' => "{$obj['description']} &#10233 Cores: {$obj['cores']}  -  Memory: {$obj['memory']} GB  -  Disk: {$obj['disk']} GB"
                    );
                }elseif ($var['variable'] == 'image'){
                    $var['items'][] = array('variable_id' => $obj['name'], 'name' => $obj['description']);
                }elseif ($var['variable'] == 'location'){
                    $var['items'][] = array('variable_id' => $obj['name'], 'name' => $obj['description']);
                }
            }
        }
        $fid = $f->addFieldCat($var);
        if ($fid) {
            $this->template->assign('fid', $fid);
            $this->template->assign('pid', $params['id']);
            $this->template->assign('vartype', $var['variable']);
        }
    }
}